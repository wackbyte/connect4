use {
    crate::{
        consts::{HEIGHT, WIDTH},
        input::Input,
        world::World,
    },
    log::{debug, error, info, trace, warn},
    pixels::{Pixels, SurfaceSize, SurfaceTexture},
    thiserror::Error,
    winit::{
        dpi::{LogicalPosition, LogicalSize, PhysicalPosition, PhysicalSize},
        event::{Event, VirtualKeyCode, WindowEvent},
        event_loop::{ControlFlow, EventLoop},
        window::{Window, WindowBuilder},
    },
};

mod consts;
mod input;
mod world;

#[derive(Error, Debug)]
enum Error {
    #[error("window: {0}")]
    Window(#[from] winit::error::OsError),
    #[error("render: {0}")]
    Render(#[from] pixels::Error),
}

#[derive(Debug)]
struct State {
    event_loop: EventLoop<()>,
    window: Window,
    pixels: Pixels,
    input: Input,
    world: World,
}

impl State {
    fn new() -> Result<Self, Error> {
        info!("initializing...");

        info!("creating event loop...");
        let event_loop = EventLoop::new();

        info!("creating window...");
        let window = {
            let size = LogicalSize::new(WIDTH as f64, HEIGHT as f64);
            WindowBuilder::new()
                .with_title("connect4")
                .with_inner_size(size)
                .with_min_inner_size(size)
                .build(&event_loop)?
        };

        info!("creating surface...");
        let pixels = {
            let window_size = window.inner_size();
            let surface_texture =
                SurfaceTexture::new(window_size.width, window_size.height, &window);
            Pixels::new(WIDTH, HEIGHT, surface_texture)?
        };

        info!("initializing input...");
        let input = Input::new();

        info!("initializing world...");
        let world = World::new();

        info!("done!");
        Ok(Self {
            event_loop,
            window,
            pixels,
            input,
            world,
        })
    }

    fn run(self) {
        let Self {
            event_loop,
            window,
            mut pixels,
            mut input,
            mut world,
        } = self;

        info!("starting the event loop!");
        event_loop.run(move |event, _, control_flow| match event {
            Event::RedrawRequested(_) => {
                world.render(pixels.get_frame());
                if let Err(error) = pixels.render() {
                    error!("{}", Error::from(error));
                    *control_flow = ControlFlow::Exit;
                    return;
                }
            }
            Event::WindowEvent { event, .. } => match event {
                WindowEvent::KeyboardInput {
                    input: keyboard, ..
                } => {
                    input.handle(&keyboard);
                    if input.quit {
                        info!("closing...");
                        *control_flow = ControlFlow::Exit;
                    }
                    world.update(&input);
                    window.request_redraw();
                }
                WindowEvent::Resized(size) => {
                    info!("resizing to ({}, {})", size.width, size.height);
                    pixels.resize_surface(size.width, size.height);
                }
                WindowEvent::CloseRequested => {
                    info!("closing...");
                    *control_flow = ControlFlow::Exit;
                }
                _ => (),
            },
            _ => (),
        });
    }
}

fn main() {
    env_logger::init_from_env(
        env_logger::Env::new()
            .filter("CONNECT4_LOG")
            .write_style("CONNECT4_LOG_STYLE"),
    );

    let state = match State::new() {
        Ok(state) => state,
        Err(error) => {
            error!("{}", error);
            return;
        }
    };
    state.run();
}
