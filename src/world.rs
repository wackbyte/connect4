use {
    crate::{
        consts::{HEIGHT, WIDTH},
        input::Input,
    },
    log::{debug, error, info, trace, warn},
};

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
enum Player {
    /// Represents either Player 1 or their piece.
    One,
    /// Represents either Player 2 or their piece.
    Two,
}

impl Player {
    pub const fn is_one(self) -> bool {
        matches!(self, Self::One)
    }

    pub const fn is_two(self) -> bool {
        matches!(self, Self::Two)
    }

    pub const fn flip(self) -> Self {
        match self {
            Self::One => Self::Two,
            Self::Two => Self::One,
        }
    }
}

#[derive(Debug, Clone)]
struct Board {
    map: [[Option<Player>; Self::HEIGHT]; Self::WIDTH],
}

impl Board {
    pub const WIDTH: usize = 7;
    pub const HEIGHT: usize = 6;
    pub const CONNECT: usize = 4;

    pub const fn new() -> Self {
        Self {
            map: [[None; 6]; 7],
        }
    }

    pub const fn get_piece(&self, x: usize, y: usize) -> Option<Player> {
        self.map[x][y]
    }

    fn get_piece_mut(&mut self, x: usize, y: usize) -> &mut Option<Player> {
        &mut self.map[x][y]
    }

    pub const fn get_column(&self, x: usize) -> [Option<Player>; Self::HEIGHT] {
        self.map[x]
    }

    fn get_column_mut(&mut self, x: usize) -> &mut [Option<Player>; Self::HEIGHT] {
        &mut self.map[x]
    }

    pub fn column_is_full(&self, x: usize) -> bool {
        self.get_column(x).iter().all(Option::is_some)
    }

    /// return `Some` if the piece successfully dropped, `None` if the column is full.
    pub fn drop(&mut self, x: usize, played: Player) -> Option<usize> {
        let mut y = Self::HEIGHT - 1;
        for piece in self.get_column_mut(x).into_iter().rev() {
            if piece.is_none() {
                *piece = Some(played);
                return Some(y);
            }
            y -= 1;
        }

        None
    }

    fn possible_x(x: isize) -> bool {
        x >= 0 && x <= ((Self::WIDTH as isize) - 1)
    }

    fn possible_y(y: isize) -> bool {
        y >= 0 && y <= ((Self::HEIGHT as isize) - 1)
    }

    fn subcheck(&self, x: usize, y: usize, dx: isize, dy: isize, target: Player) -> usize {
        let mut nx = x as isize;
        let mut ny = y as isize;
        let mut count = 0;

        for _ in 0..(Self::CONNECT - 1) {
            nx += dx;
            ny += dy;
            if Self::possible_x(nx) && Self::possible_y(ny) {
                if let Some(piece) = self.get_piece(nx as usize, ny as usize) {
                    if piece == target {
                        count += 1;
                    } else {
                        break;
                    }
                } else {
                    break;
                }
            } else {
                break;
            }
        }

        count
    }

    /// i was going to explain how this works but honestly it's too late for this
    pub fn check(&self, x: usize, y: usize, target: Player) -> bool {
        const HALF_DIRECTIONS: [(isize, isize); 4] = [(1, 0), (1, 1), (0, 1), (-1, 1)];

        if let Some(piece) = self.get_piece(x, y) {
            if piece == target {
                for (dx, dy) in HALF_DIRECTIONS {
                    let total =
                        self.subcheck(x, y, dx, dy, target) + self.subcheck(x, y, -dx, -dy, target);
                    if total >= (Self::CONNECT - 1) {
                        return true;
                    }
                }
            }
        }

        false
    }
}

#[derive(Debug)]
pub struct World {
    turn: Player,
    column: usize,
    board: Board,
    winner: Option<Player>,
}

impl World {
    pub fn new() -> Self {
        Self {
            turn: Player::One,
            column: 3,
            board: Board::new(),
            winner: None,
        }
    }

    pub fn update(&mut self, input: &Input) {
        if input.reset && !input.last_reset {
            *self = Self::new();
        }

        if self.winner.is_none() {
            if input.left && !input.last_left {
                info!("input: left");
                self.column = self.column.saturating_sub(1);
            }

            if input.right && !input.last_right {
                info!("input: right");
                self.column = (self.column + 1).min(Board::WIDTH - 1);
            }

            if input.drop && !input.last_drop {
                info!("input: drop");
                if let Some(row) = self.board.drop(self.column, self.turn) {
                    if self.board.check(self.column, row, self.turn) {
                        info!("winner: {:?}", self.turn);
                        self.winner = Some(self.turn);
                    } else {
                        let old = self.turn;
                        self.turn = self.turn.flip();
                        info!("switch {:?} -> {:?}", old, self.turn);
                    }
                }
            }
        }
    }

    pub fn render(&mut self, frame: &mut [u8]) {
        const COLOR1_LIGHT: [u8; 4] = [91, 127, 234, 255];
        const COLOR1_DARK: [u8; 4] = [61, 85, 158, 255];
        const COLOR2_LIGHT: [u8; 4] = [234, 127, 91, 255];
        const COLOR2_DARK: [u8; 4] = [158, 85, 61, 255];

        const PIECE_WIDTH: usize = (WIDTH as usize) / 8;
        const PIECE_HEIGHT: usize = (HEIGHT as usize) / 8;
        const PIECE_SPACE_X: usize = 2;
        const PIECE_SPACE_Y: usize = 2;
        const PIECE_OFFSET_X: usize = ((WIDTH as usize)
            - ((PIECE_WIDTH * Board::WIDTH) + (PIECE_SPACE_X * (Board::WIDTH - 1))))
            / 2;
        const PIECE_OFFSET_Y: usize = (HEIGHT as usize)
            - ((PIECE_HEIGHT * Board::HEIGHT) + (PIECE_SPACE_Y * (Board::HEIGHT - 1)));
        const PIECE_BORDER_WIDTH: usize = 2;
        const PIECE_BORDER_HEIGHT: usize = 2;

        const SELECT_WIDTH: usize = PIECE_WIDTH / 2;
        const SELECT_HEIGHT: usize = PIECE_HEIGHT / 4;
        const SELECT_Y: usize = 16;

        const GRID_HEIGHT: usize =
            (PIECE_HEIGHT + PIECE_SPACE_Y) * (Board::HEIGHT - 1) + PIECE_HEIGHT;
        const GRID_COLOR: [u8; 4] = [255; 4];

        const fn player_color_light(player: Player) -> [u8; 4] {
            match player {
                Player::One => COLOR1_LIGHT,
                Player::Two => COLOR2_LIGHT,
            }
        }

        const fn player_color_dark(player: Player) -> [u8; 4] {
            match player {
                Player::One => COLOR1_DARK,
                Player::Two => COLOR2_DARK,
            }
        }

        #[inline]
        fn clear(frame: &mut [u8], color: [u8; 4]) {
            for pixel in frame.chunks_exact_mut(4) {
                pixel.copy_from_slice(&color);
            }
        }

        #[inline]
        fn blit(frame: &mut [u8], x: usize, y: usize, pixel: [u8; 4]) {
            let i = x * 4 + y * WIDTH as usize * 4;
            frame[i] = pixel[0];
            frame[i + 1] = pixel[1];
            frame[i + 2] = pixel[2];
            frame[i + 3] = pixel[3];
        }

        #[inline]
        fn vline(frame: &mut [u8], x: usize, y: usize, h: usize, pixel: [u8; 4]) {
            for i in y..(y + h) {
                blit(frame, x, i, pixel);
            }
        }

        #[inline]
        fn hline(frame: &mut [u8], x: usize, y: usize, w: usize, pixel: [u8; 4]) {
            for i in x..(x + w) {
                blit(frame, i, y, pixel);
            }
        }

        #[inline]
        fn rect(frame: &mut [u8], x: usize, y: usize, w: usize, h: usize, pixel: [u8; 4]) {
            for i in x..(x + w) {
                for j in y..(y + h) {
                    blit(frame, i, j, pixel);
                }
            }
        }

        clear(frame, [0, 0, 0, 255]);

        match self.winner {
            None => {
                rect(
                    frame,
                    PIECE_OFFSET_X - PIECE_SPACE_X,
                    PIECE_OFFSET_Y,
                    PIECE_SPACE_X,
                    GRID_HEIGHT,
                    GRID_COLOR,
                );
                for i in 1..(Board::WIDTH + 1) {
                    let x = PIECE_OFFSET_X + ((PIECE_WIDTH + PIECE_SPACE_X) * i);
                    rect(
                        frame,
                        x - PIECE_SPACE_X,
                        PIECE_OFFSET_Y,
                        PIECE_SPACE_X,
                        GRID_HEIGHT,
                        GRID_COLOR,
                    );
                }

                for x in 0..Board::WIDTH {
                    for y in 0..Board::HEIGHT {
                        if let Some(piece) = self.board.get_piece(x, y) {
                            let offset_x = PIECE_OFFSET_X + ((PIECE_WIDTH + PIECE_SPACE_X) * x);
                            let offset_y = PIECE_OFFSET_Y + ((PIECE_HEIGHT + PIECE_SPACE_Y) * y);
                            rect(
                                frame,
                                offset_x,
                                offset_y,
                                PIECE_WIDTH,
                                PIECE_HEIGHT,
                                player_color_dark(piece),
                            );
                            rect(
                                frame,
                                offset_x + PIECE_BORDER_WIDTH,
                                offset_y + PIECE_BORDER_WIDTH,
                                PIECE_WIDTH - (PIECE_BORDER_WIDTH * 2),
                                PIECE_HEIGHT - (PIECE_BORDER_HEIGHT * 2),
                                player_color_light(piece),
                            );
                        }
                    }
                }

                rect(
                    frame,
                    PIECE_OFFSET_X
                        + ((PIECE_WIDTH + PIECE_SPACE_X) * self.column)
                        + (PIECE_WIDTH / 2)
                        - (SELECT_WIDTH / 2),
                    SELECT_Y,
                    SELECT_WIDTH,
                    SELECT_HEIGHT,
                    player_color_light(self.turn),
                );
            }
            Some(winner) => {
                rect(
                    frame,
                    ((WIDTH as usize) / 2) - PIECE_WIDTH,
                    ((HEIGHT as usize) / 2) - PIECE_HEIGHT,
                    PIECE_WIDTH * 2,
                    PIECE_HEIGHT * 2,
                    player_color_dark(winner),
                );
                rect(
                    frame,
                    (((WIDTH as usize) / 2) - PIECE_WIDTH) + (PIECE_BORDER_WIDTH * 2),
                    (((HEIGHT as usize) / 2) - PIECE_HEIGHT) + (PIECE_BORDER_WIDTH * 2),
                    (PIECE_WIDTH * 2) - (PIECE_BORDER_WIDTH * 4),
                    (PIECE_HEIGHT * 2) - (PIECE_BORDER_HEIGHT * 4),
                    player_color_light(winner),
                );
            }
        }
    }
}

impl Default for World {
    fn default() -> Self {
        Self::new()
    }
}
