use winit::event::{ElementState, KeyboardInput, VirtualKeyCode};

#[derive(Debug, Clone)]
pub struct Input {
    pub left: bool,
    pub last_left: bool,
    pub right: bool,
    pub last_right: bool,
    pub drop: bool,
    pub last_drop: bool,
    pub reset: bool,
    pub last_reset: bool,
    pub quit: bool,
}

impl Input {
    pub fn new() -> Self {
        Self {
            left: false,
            last_left: false,
            right: false,
            last_right: false,
            drop: false,
            last_drop: false,
            reset: false,
            last_reset: false,
            quit: false,
        }
    }

    pub fn handle(&mut self, keyboard: &KeyboardInput) {
        self.last_left = self.left;
        self.last_right = self.right;
        self.last_drop = self.drop;
        self.last_reset = self.reset;

        self.left = false;
        self.right = false;
        self.reset = false;
        self.drop = false;

        if let KeyboardInput {
            virtual_keycode: Some(key),
            state,
            ..
        } = keyboard
        {
            let pressed = matches!(state, ElementState::Pressed);
            let button = match key {
                VirtualKeyCode::Left | VirtualKeyCode::A => &mut self.left,
                VirtualKeyCode::Right | VirtualKeyCode::D => &mut self.right,
                VirtualKeyCode::Space | VirtualKeyCode::Return => &mut self.drop,
                VirtualKeyCode::R => &mut self.reset,
                VirtualKeyCode::Escape => &mut self.quit,
                _ => {
                    return;
                }
            };
            *button = pressed;
        }
    }
}

impl Default for Input {
    fn default() -> Self {
        Self::new()
    }
}
