{
  description = "A simple four-in-a-row game.";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    rust-overlay.url = "github:oxalica/rust-overlay";
  };

  outputs = { nixpkgs, rust-overlay, ... }:
    let system = "x86_64-linux";
    in
    {
      devShell.${system} =
        let
          pkgs = import nixpkgs {
            inherit system;
            overlays = [ rust-overlay.overlay ];
          };
        in
        (({ pkgs, ... }:
          pkgs.mkShell {
            buildInputs = with pkgs; [
              openssl
              libGL
              vulkan-loader
              wayland
              wayland-protocols
              libxkbcommon
              xorg.libX11
              xorg.libXrandr
              xorg.libXi
              xorg.libXcursor
            ];
            nativeBuildInputs = with pkgs; [
              rustc
              cargo
              pkgconfig
              clang
              nixpkgs-fmt
            ];

            RUST_SRC_PATH = "${pkgs.rust.packages.stable.rustPlatform.rustLibSrc}";
            PKG_CONFIG_PATH = "${pkgs.pkgconfig}/lib/pkgconfig";
            LD_LIBRARY_PATH =
              "${pkgs.vulkan-loader}/lib:${pkgs.xorg.libX11}/lib:${pkgs.xorg.libXcursor}/lib:${pkgs.xorg.libXi}/lib:${pkgs.xorg.libXrandr}/lib:${pkgs.libGL}/lib:$LD_LIBRARY_PATH";
          }) { pkgs = pkgs; });
    };
}
