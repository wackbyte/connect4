# connect4

A simple four-in-a-row game.

Built with [`pixels`](https://github.com/parasyte/pixels) and [`winit`](https://github.com/rust-windowing/winit).

## controls

```
Left  | A     => "left"
Right | D     => "right"
Space | Enter => "drop"
R             => "reset"
Escape        => "quit"
```

## notes

Text rendering would have been a little too complex for not only the scope of this project
but also my very limited and quickly closing time window for completion, so when you win,
it just blits your piece to the center of the screen.

Also, the CPU usage seems *sus*piciously high for such a simple program. It's probably because I didn't put a frame timer in. oops lol
